import mainModule from './app/main.module';
import Chartist from 'chartist'
require('bootstrap');
import './style.scss';
import 'bootstrap/dist/js/bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import 'angular-loading-bar/build/loading-bar.min.js';
import 'angular-loading-bar/build/loading-bar.min.css';
import 'toastr/build/toastr.css';
import 'select2-bootstrap-theme/dist/select2-bootstrap.min.css'

angular.bootstrap(document.body, [mainModule], { strictDi: true });
