export default class FormController {

    constructor(ClienteService, $stateParams, $state,toastrService) {
        this.toastrService = toastrService
        this.service = ClienteService;
        this.$state = $state;
        if ($stateParams.id) {
            this.service.getOne($stateParams.id)
                .then(registro => {
                    this.record = registro;
                }).catch( error => {
                    this.toastrService.sucesso(error);
                });
        }
    }

    save() {
        this.service.save(this.record)
            .then(() => {
                this.toastrService.sucesso('Cliente adicionado com sucesso!');
                this.$state.go('app.cliente.list')
            }).catch(console.error);
    }
}

FormController.$inject = [
    'ClienteService', 
    '$stateParams',
    '$state',
    'toastrService'
];
