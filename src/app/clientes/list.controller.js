export default class ListController {

    constructor(ClienteService) {
        this.service = ClienteService;
        this.filtro;
        this.records = [];

        ClienteService.getAll()
            .then(records => {
                this.records = records;
            }).catch(erro => {
                console.log(erro);
            });


    }

    filtrar() {
        console.log(this.filterValue)
        let filtro = '?filterField=cliente&filterValue='+this.filterValue;

        this.service.find(filtro)
            .then(records => {
                this.records = records
   
            }).catch(erro => {
                console.log(erro);
            });
    }

    // abrirModalCadastroUsuario() {
    //     this.$uibModal.open({
    //       animation: true,
    //       ariaLabelledBy: 'Cadastro de usuário',
    //       ariaDescribedBy: 'modal-body',
    //       template: require('../views/os/inf.html'),
    //       controller: NovoUsuarioController,
    //       controllerAs: 'vm',
    //       size: 'md',
    //       resolve: {
    //         Restangular: this.Restangular,
    //         toastrService: this.toastrService
    //       }
    //     });
    //   }
    }



ListController.$inject = ['ClienteService','$uibModal',]