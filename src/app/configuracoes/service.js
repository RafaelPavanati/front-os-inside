export default class ConfiguracaoService {

    constructor($http, BASE_URL) {
        this.$http = $http;
        this.baseUrl = `${BASE_URL}/config`;
    }
  
    getAll() {
        return this.$http.get(this.baseUrl)
            .then(response => response.data);
    }


    save(data) {
        if (data.id) {
            return this.$http.put(`${this.baseUrl}/${data.id}`, data)
                .then(response => response.data);
        } else {
            return this.$http.post(this.baseUrl, data)
                .then(response => response.data);
        }
    }
}

ConfiguracaoService.$inject = ['$http', 'BASE_URL'];