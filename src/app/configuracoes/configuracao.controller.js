export default class ConfiguracaoController {

    constructor(ConfiguracaoService,toastrService, $state) {
        this.service = ConfiguracaoService;
        this.toastrService = toastrService;
        this.$state = $state;
        this.service.getAll()
        .then(records => {
            this.record = records[0];
        }).catch(erro => {
            console.log(erro);
        });

        
    }
    
    save() {
        this.service.save(this.record)
            .then(() => {
                this.$state.go('app.config')
                this.toastrService.sucesso('Configuração adicionada com sucesso!');
            }).catch(console.error);
    }
}

ConfiguracaoController.$inject = [
'ConfiguracaoService',
'toastrService',
'$state'
];
