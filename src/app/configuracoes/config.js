import ConfiguracaoController  from './configuracao.controller'
import ConfiguracaoService  from './service'


export const Config = (modulo) => {
    modulo.service('ConfiguracaoService', ConfiguracaoService);
  return ['$stateProvider', config];
};

function config ($stateProvider) {
  $stateProvider
    .state('app.config', {
      template: require('@views/config/config.html'),
      url: 'config',
      controller: ConfiguracaoController,
      controllerAs: 'vm'
    });
}