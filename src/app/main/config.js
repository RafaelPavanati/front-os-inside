import MainController from './controller';
import LoginController from './login.controller';
import MenuController from './menu.controller';

import '../../assets/bootstrap-solid.svg';

import DashBoardService from './service';

// export const clienteConfig = (modulo) => {



export const mainConfig = (modulo) => {
  modulo.service('DashBoardService', DashBoardService);
  return ['$stateProvider', '$urlRouterProvider', config];
};

function config ($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/');
  $stateProvider
    .state('app', {
      template: require('@views/template.html'),
      redirectTo: 'app.dashboard',
      controller: MenuController,
      controllerAs: 'vm',
      url: '/'
    })
    .state('app.dashboard', {
      template: require('@views/main/component.html'),
      controller: MainController,
      controllerAs: 'vm',
      url: 'dashboard'
    })
    .state('login', {
      template: require('@views/login.html'),
      controller: LoginController,
      controllerAs: 'vm',
      url: '/login'
    });
};

mainConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
