import Chart from 'chart.js';
import SemMovimentacaoController from '../order-servico/semMovimentacao.controller';

export default class MainController {

    constructor(DashBoardService, $uibModal) {
        this.$uibModal = $uibModal;

        DashBoardService.getAll()
            .then(records => {

                this.dashBoardInf = records;
         
                var data = {
                    datasets: [{
                        data: [records.osParada ? records.osParada.length : 0]
                    }],
                    labels: [
                        'Ordem de servicos'
                    ]
              
                
                };
                var ctx2 = document.getElementById("myChart2");
                this.myDoughnutChart = new Chart(ctx2, {
                    type: 'doughnut',
                    data: data
                });


                // var ctx = document.getElementById("myChart");
                // this.myChart = new Chart(ctx, {
                //     type: 'bar',
                //     data: {
                //         labels: [records.meses[5],
                //          records.meses[4],
                //           records.meses[3], 
                //           records.meses[2], 
                //           records.meses[1], 
                //           records.meses[0]],
                //         datasets: [{
                //             label: 'Retornos',
                //             data: [records.retornoMeses[5],
                //              records.retornoMeses[4],
                //               records.retornoMeses[3], 
                //               records.retornoMeses[2],
                //                records.retornoMeses[1], 
                //                records.retornoMeses[0]],
                //             backgroundColor: [
                //                 'rgba(255, 99, 132, 0.2)',
                //                 'rgba(54, 162, 235, 0.2)',
                //                 'rgba(255, 206, 86, 0.2)',
                //                 'rgba(75, 192, 192, 0.2)',
                //                 'rgba(153, 102, 255, 0.2)',
                //                 'rgba(255, 159, 64, 0.2)'
                //             ],
                //             borderColor: [
                //                 'rgba(255,99,132,1)',
                //                 'rgba(54, 162, 235, 1)',
                //                 'rgba(255, 206, 86, 1)',
                //                 'rgba(75, 192, 192, 1)',
                //                 'rgba(153, 102, 255, 1)',
                //                 'rgba(255, 159, 64, 1)'
                //             ],
                //             borderWidth: 1
                //         }]
                //     },
                //     options: {
                //         scales: {
                //             yAxes: [{
                //                 ticks: {
                //                     beginAtZero: true
                //                 }
                //             }]
                //         }
                //     }
                // });

           

                this.options = {
                    chart: {
                        type: 'discreteBarChart',
                        height: 250,
                        margin: {
                            top: 20,
                            right: 20,
                            bottom: 60,
                            left: 55
                        },
                        x: function (d) { return d.label; },
                        y: function (d) { return d.value; },
                        showValues: true,
                        valueFormat: function (d) {
                            return d3.format(',.0f')(d);
                        },
                        transitionDuration: 100000,
                        xAxis: {
                            axisLabel: ''
                        },
                        yAxis: {
                            axisLabel: 'Y Axis',
                            axisLabelDistance: 30
                        }
                    }
                };

                /* Chart data */
                this.data = [{
                    key: "Cumulative Return",
                    values: [
                        { "label": "Orcamento", "value": records.orcamentos },
                        { "label": "Garantias", "value": records.garantias },
                        { "label": "Retornos", "value": records.retornos },
                    ]
                }]
            }
            )
    }

    abrirModalInformacoes() {

        let template = require('../views/os/lista-sem-movimentacao.html');
        this.modalInstance = this.$uibModal.open({
            template,
            controller: SemMovimentacaoController,
            controllerAs: 'vm',
            size: 'lg',
            resolve: {
                os: this.dashBoardInf,
            }
        });

    }

    close(){
        this.modalInstance.close
    }
    class() {
        console.log("sadas")
        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
    }
}


MainController.$inject = [
    'DashBoardService',
    '$uibModal'
];