export default class MenuController {
    constructor() {
        this.itens = [
            {
                state:'app.dashboard',
                icon: 'home',
                name: 'Dashboard'
            },{
                state:'app.cliente',
                icon: 'person',
                name: 'Clientes'
            },{
                state:'app.os',
                icon: 'list_alt',
                name: 'Serviços'
            },
            {
                state:'app.config',
                icon: 'settings',
                name: 'Configurações'
            },
            {
                state:'login',
                icon: 'directions_run',
                name: 'Sair'
            }
        ]
    }
    class(){
        console.log("sadas")
        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
    }
}