export default class DashBoardService {

    constructor($http, BASE_URL) {
        this.$http = $http;
        this.baseUrl = `${BASE_URL}/dashboard/inf`;
    }

    getAll() {
        return this.$http.get(this.baseUrl)
            .then(response => response.data);
    }

    getOne(id) {
        return this.$http.get(`${this.baseUrl}/${id}`)
            .then(response => response.data);
    }

  

}

DashBoardService.$inject = ['$http', 'BASE_URL'];