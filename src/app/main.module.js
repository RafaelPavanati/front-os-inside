import angular from 'angular';
import Chart from 'chart.js';
import { default as uiRouter } from '@uirouter/angularjs';

import { default as uiSelect } from 'ui-select'
import { default as ngSanitize } from 'angular-sanitize'
import { default as nGFusioncharts } from 'angularjs-fusioncharts';
import { default as nvd3 } from 'angular-nvd3';







import toastrService from './toastr.service';
import diretivas from './diretivas';
import { default as uiBootstrap } from 'angular-ui-bootstrap';
import { default as angularLoadingBar } from 'angular-loading-bar';
import { default as NgInputMask } from 'angular-input-masks';




import { Config } from './configuracoes/config';
import { mainConfig } from './main/config';
import { clienteConfig } from './clientes/config';
import { OrdemServicoConfig } from './order-servico/config';




const modulo = angular
  .module('app', [uiRouter, diretivas, uiSelect, ngSanitize, nvd3, uiBootstrap, angularLoadingBar, NgInputMask]);

export default modulo
  .config(Config(modulo))
  .config(mainConfig(modulo))
  .service('toastrService', toastrService)
  .config(clienteConfig(modulo))
  .config(OrdemServicoConfig(modulo))
  .constant('BASE_URL', 'http://localhost:8080/api')
  .name;
