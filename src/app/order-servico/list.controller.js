import InfoOsController from '../order-servico/inf.controller';
export default class ListController {

    constructor(OrdemServicoService, $uibModal) {
        this.service = OrdemServicoService;
        this.$uibModal = $uibModal;
        this.page = 1;
        this.filtroItem = 'numero';
        this.filterValue = '';
        this.cols = [{
            label: 'Numero OS',
            value: 'id'
        }, {
            label: 'Equipamento',
            value: 'nome'
        }, {
            label: 'Modelo',
            value: 'modelo'
        }, {
            label: 'Cliente',
            value: 'cliente.nome'
        }
            , {
            label: 'Origem',
            value: 'tipoServico'
        }
            , {
            label: 'Situacao',
            value: 'situacao'
        }
        ];

        this.records = [];

        OrdemServicoService.getAll(this.page)
            .then(records => {
                this.records = records;
            }).catch(erro => {
                console.log(erro);
            });
    }
    findAll() {
        this.service.getAll(this.page)
            .then(records => {
                this.records = records;
            }).catch(erro => {
                console.log(erro);
            });
    }

    selectFiltro(data) {

        this.filtroItem = data;
    }
    abrirModalInformacoes(id) {

        let template = require('../views/os/inf.html');
        var modalInstance = this.$uibModal.open({
            template,
            controller: InfoOsController,
            controllerAs: 'vm',
            size: 'lg',
            resolve: {
                id: id
            }
        });

    }
    imprimir(data){
        this.service.imprimir(data)
        .then(records => {
          
        }).catch(erro => {
            console.log(erro);
        });
    }
    pagenator(data) {
        if(data === 0){
            this.page = 1;
        }else{
            this.page = this.page + data;
            console.log(this.page)
        }
  
    }
    filtrar() {

        if (this.filterValue == '') {
            this.service.getAll(this.page)
                .then(records => {
                    this.records = records;
                }).catch(erro => {
                    console.log(erro);
                });
        }
        let filtro;

        if (this.filtroItem === 'numero') {
            this.service.getOne(this.filterValue)
                .then(records => {
                    this.records = [];
                    this.records.push(records);

                }).catch(erro => {
                    console.log(erro);
                });
        } else {
            if (this.filtroItem === 'marca') {
                filtro = 'marca';
            }
            if (this.filtroItem === 'equipamento') {
                filtro = 'nome';
            }
            if (this.filtroItem === 'cliente') {
                filtro = 'cliente';
            }
            if (this.filtroItem === 'controle') {
                filtro = 'controle';
            }
            this.service.find(filtro ? filtro : this.filtroItem, this.filterValue)
                .then(records => {
                    this.records = records
                }).catch(erro => {
                    console.log(erro);
                });
        }
    }
}

ListController.$inject = ['OrdemServicoService', '$uibModal']