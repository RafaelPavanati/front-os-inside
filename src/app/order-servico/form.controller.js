export default class FormController {

    constructor(OrdemServicoService,ClienteService, $stateParams, $state , $timeout,toastrService) {
        this.toastrService = toastrService;

        ClienteService.getAll()
        .then(records => {
            this.clientes  = records;
        }).catch(erro => {
            console.log(erro);
        });
        this.service = OrdemServicoService;
        this.$state = $state;
        if ($stateParams.id) {
            this.service.getOne($stateParams.id)
                .then(registro => {
                 
                    this.record = registro;
                }).catch(console.error);
        }
    }

    save() {
        console.log(this.record.situacao)
    
        this.service.save(this.record)
            .then(() => {
                this.toastrService.sucesso('Ordem de serviço adicionada com sucesso!');
                this.$state.go('app.os.list')
            }).catch(console.error);
    }
}

FormController.$inject = [
    'OrdemServicoService', 
    'ClienteService',
    '$stateParams',
    '$state',
    '$timeout',
    'toastrService'
];
