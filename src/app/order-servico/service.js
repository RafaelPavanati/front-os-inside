export default class OrdemServicoService {

    constructor($http, BASE_URL) {
        this.$http = $http;
        this.baseUrl = `${BASE_URL}/ordem-servico`;
    }

    imprimir(data){
        return this.$http.post(`${this.baseUrl}/imprimir`, data)
        .then(response => response.data);
    }

    find(filtro, valor){
        return this.$http.get(`${this.baseUrl}?filterField=${filtro}&filterValue=${valor}&order=abertura desc`)
        .then(response => response.data);
    }

    getAll(page) {
        return this.$http.get(`${this.baseUrl}?page=${page}&order=abertura desc`)
            .then(response => response.data);
    }

    getOne(id) {
        return this.$http.get(`${this.baseUrl}/${id}`)
            .then(response => response.data);
    }

    save(data) {
        if (data.id) {
            return this.$http.put(`${this.baseUrl}/${data.id}`, data)
                .then(response => response.data);
        } else {
            return this.$http.post(this.baseUrl, data)
                .then(response => response.data);
        }
    }

    remove(id) {
        return this.$http.delete(`${this.baseUrl}/${id}`);
    }

}

OrdemServicoService.$inject = ['$http', 'BASE_URL'];